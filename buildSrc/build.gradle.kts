import extensions.myExtension

plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
    google()
    myExtension()
}